package org.example.songviewer;

import java.util.List;

public interface SongFetchable {

    public List<Song> fetchAllSongs();


}
