package org.example.songviewer;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class SongViewerService {

    private final List<SongFetchable> services;


    public SongViewerService(List<SongFetchable> services) {
        this.services = services;
    }

    public List<Song> ViewAllSongs() {
        List<Song> songsToView = new ArrayList<>();
        services.forEach(songService -> songsToView.addAll(songService.fetchAllSongs()));
        return songsToView;
    }


}
