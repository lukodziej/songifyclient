package org.example.itunes.service;

import lombok.extern.log4j.Log4j2;
import org.example.itunes.proxy.ItunesProxy;
import org.example.itunes.proxy.ItunesResponse;
import org.example.itunes.proxy.ItunesResult;
import org.example.songviewer.Song;
import org.example.songviewer.SongFetchable;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

@Log4j2
@Service
public class ItunesService implements SongFetchable {

    ItunesProxy iTunesClient;

    ItunesMapper itunesMapper;

    public ItunesService(ItunesProxy iTunesClient, ItunesMapper itunesMapper) {
        this.iTunesClient = iTunesClient;
        this.itunesMapper = itunesMapper;
    }

    private List<ItunesResult> fetchShawnMendesSongsFromItunes() {
        String json = iTunesClient.makeGetRequest("shawnmendes", 3);
        if (json == null) {
            log.error("jsonSongs was null");
            return Collections.emptyList();
        }
        ItunesResponse shawnMendesResponse = itunesMapper.mapJsonToItunesResponse(json);
        log.info(shawnMendesResponse);
        return shawnMendesResponse.results();
    }

    public List<Song> fetchAllSongs() {
        List<ItunesResult> itunesResults = fetchShawnMendesSongsFromItunes();
        return itunesResults.stream()
                .map(itunesResult -> new Song(itunesResult.trackName(), itunesResult.artistName()))
                .toList();
    }
}
