package org.example.restclient.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.log4j.Log4j2;
import org.example.restclient.proxy.LocalServerResult;
import org.springframework.stereotype.Component;

import java.util.Collections;

@Log4j2
@Component
public class LocalServerServiceMapper {
    private final ObjectMapper objectMapper;

    public LocalServerServiceMapper(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    LocalServerResult mapJsonToSampleShawnMendesResponse(String json) {

        try {
            return objectMapper.readValue(json, LocalServerResult.class);
        } catch (JsonProcessingException e) {
            log.error(e.getMessage());
            return new LocalServerResult("empty", Collections.emptyMap());
        }
    }
}
