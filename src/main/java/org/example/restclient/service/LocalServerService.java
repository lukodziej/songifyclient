package org.example.restclient.service;

import lombok.extern.log4j.Log4j2;
import org.example.restclient.proxy.LocalServerProxy;
import org.example.restclient.proxy.LocalServerResponse;
import org.example.restclient.proxy.LocalServerResult;
import org.example.songviewer.Song;
import org.example.songviewer.SongFetchable;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Map;

@Log4j2
@Service
public class LocalServerService implements SongFetchable {

    private final LocalServerProxy localServerProxy;
    private final LocalServerServiceMapper localServerServiceMapper;

    public LocalServerService(LocalServerProxy localServerProxy, LocalServerServiceMapper localServerServiceMapper) {
        this.localServerProxy = localServerProxy;
        this.localServerServiceMapper = localServerServiceMapper;
    }

    private Map<Integer,LocalServerResponse> fetchAllSongsFromLocalServer() {
        String jsonSongs = localServerProxy.makeGetRequest();
        if (jsonSongs == null) {
            log.error("jsonSongs was null");
            return Collections.emptyMap();
        }
        LocalServerResult localServerResult = localServerServiceMapper.mapJsonToSampleShawnMendesResponse(jsonSongs);
        log.info("LocalServerService fetched: " + localServerResult.songs());

        return localServerResult.songs();
    }

    public List<Song> fetchAllSongs() {
        Map<Integer,LocalServerResponse> localResults = fetchAllSongsFromLocalServer();
        return localResults.values().stream()
                .map(localServerResponse -> new Song(localServerResponse.name(), localServerResponse.artist()))
                .toList();
    }

    public String fetchOneSong(Integer id){
        return localServerProxy.makeGetOneSongRequest(id);
    }

    public String deleteSingleSong(Integer id){
        return localServerProxy.makeDeleteRequest(id);
    }

    public String addSong(String name, String artist){
        return localServerProxy.makePostRequest(name,artist);
    }

    public String modifySong(String name,String artist,Integer id){
        return localServerProxy.makePutRequest(name,artist,id);
    }

}
