package org.example.restclient.proxy;


public record LocalServerRequest(String songName, String artist) {
}
