package org.example.restclient.proxy;

import java.util.Map;

public record LocalServerResult(
        String message,
        Map<Integer, LocalServerResponse> songs
) {
}
