package org.example;

import lombok.extern.log4j.Log4j2;
import org.example.restclient.service.LocalServerService;
import org.example.songviewer.SongViewerService;
import org.springframework.stereotype.Component;

@Log4j2
@Component
public class MainApplicationRunner {

    private final SongViewerService songViewerService;

    private final LocalServerService localServerService;

    public MainApplicationRunner(SongViewerService songViewerService, LocalServerService localServerService) {
        this.songViewerService = songViewerService;
        this.localServerService = localServerService;
    }

    public void start() {
        log.info(songViewerService.ViewAllSongs());
        log.info(localServerService.deleteSingleSong(2));
        log.info(songViewerService.ViewAllSongs());
        log.info(localServerService.addSong("Elo", "Peja"));
        log.info(localServerService.fetchAllSongs());
        log.info(localServerService.modifySong("nowa", "change", 1));
        log.info(localServerService.fetchAllSongs());
        log.info(localServerService.fetchOneSong(1));
    }
}
