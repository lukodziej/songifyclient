package org.example.fromfilesongreader;

public record FileSong (String artistName, String trackName){
}
